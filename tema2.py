
from functools import reduce
import operator
import sys

import cmath
import numpy
from scipy import linalg, array, matrix

n = 3
b = [1, 2, 3]
A = [[1, 2.5, 3],
     [2.5, 8.25, 15.5],
     [3, 15.5, 43]]
epsilon = 10**(-10)

#
# def matrix():
#     # lines = input("insert number of lines: ")
#     # columns = input("insert number if columns: ")
#     # if lines != columns:
#     #     print("The matrix is not n x n")
#     #     return 0
#     lines=3
#     columns =3
#     global n
#     n = int(lines)
#
#     matrix = [[rn.randrange(0.0, 100.0) for items_of_line in range(int(lines))] for items_of_columns in
#               range(int(columns))]
#     sMatrix = np.tril(matrix) + np.tril(matrix, -1).T
#
#     if sMatrix.all() == sMatrix.T.all() and np.linalg.det(sMatrix) != 0:
#         return sMatrix
#     else:
#         print("is not symmetric and piitiv def.")
#
# A = matrix()

def GetLAndD():
    D = [[0.0] for i in range(n)]

    # Perform the Cholesky decomposition
    for p in range(n):
        D[p] = A[p][p] - SumPreviousElements(p, p, D, A)
        for i in range(p + 1, n):
            if (numpy.math.fabs(D[p]) > epsilon):
                A[i][p] = (A[i][p] - SumPreviousElements(p, i, D, A)) / float(D[p])
            else:
                print("nu se poate face impartirea")
                sys.exit()
    return D


def SumPreviousElements(p, i, D, L):
    sum = 0
    for k in range(p):
        sum += D[k] * L[i][k] * L[p][k]
    return sum


def SumSubstituteDirectElements(i, L, x):
    sum = 0
    for j in range(i):
        sum += L[i][j] * x[j]
    return sum


def SumSubstituteIndirectElements(i, L, x):
    sum = 0
    for j in range(i + 1, n):
        sum += L[j][i] * x[j]
    return sum


def DetD(D):
    return reduce(operator.mul, D, 1)


def SubstitutieCholeski(L, D, b):
    z = [[0.0] for i in range(n)]
    y = [[0.0] for i in range(n)]
    x = [[0.0] for i in range(n)]
    for i in range(n):
        z[i] = b[i] - SumSubstituteDirectElements(i, L, z)
    for i in range(n):
        if(numpy.math.fabs(D[i])>epsilon):
            y[i] = z[i] / D[i]
        else:
            print("nu se poate face impartirea")
            sys.exit()
    for i in range(n - 1, -1, -1):
        x[i] = y[i] - SumSubstituteIndirectElements(i, L, x)
    return x

def ReinitializeA(A):
    A[1][0] = A[0][1]
    A[2][0] = A[0][2]
    A[2][1] = A[1][2]
    return A


def numpyChol(A,b):
    L = linalg.cholesky(matrix(A),lower = True)
    print(L)
    U = linalg.cholesky(matrix(A),lower = False)
    print(U)
    print(numpy.dot(L,U))
    print(linalg.solve(A, b))

def NormaEuclidiana(A, x, b):
    resultAx = numpy.dot(A,x)
    result=[]
    for i in range(n):
        result.append(resultAx[i]-b[i])
    suma =0
    for i in range(n):
        suma+=result[i]**2
    return cmath.sqrt(suma).real


print("#1")
resultD = GetLAndD()
print("L:")
for i in range(n):
    print(A[i])
print("D:")
print(resultD)

print("\n#2")
print("Determinant:", DetD(resultD))

print("\n#3")
resultSustitutie = SubstitutieCholeski(A,resultD,b)
print("XChol:", resultSustitutie)

print("\n#4")
A = ReinitializeA(A)
numpyChol(A,b)

print("\n#5")
print(epsilon)
print NormaEuclidiana(A,resultSustitutie,b)

